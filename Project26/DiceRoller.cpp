#include "DiceRoller.h"

DiceRoller::DiceRoller()
{
	dice_sum = 0;
	dice_blocked_sum = 0;
	diceValuesSum();
	blockingDice();
}

void DiceRoller::diceValuesSum()
{
	int index = 1;
	for (Dice d : diceArray)
	{
			cout << "Valeur de " << index << ": " << d.getName() << endl;
			dice_sum += d.getValue();
			index++;
	}
	cout << endl << "Somme totale: " << dice_sum << endl << endl;
}

void DiceRoller::blockingDice()
{
	int random_number = rand() % 6;
	cout << "Chiffre aleatoire: " << random_number << endl << endl;
	int index = 1;
	for (Dice d : diceArray)
	{
		if (d.getNumber() == random_number)
		{
			cout << "De " << index << ": bloque" << endl;
			dice_blocked_sum += d.getValue();
		}	
		else
		{
			cout << "De " << index << ": non bloque" << endl;
		}			
		index++;
	}
	cout << endl << "Somme totale des bloques: " << dice_blocked_sum << endl;
}

