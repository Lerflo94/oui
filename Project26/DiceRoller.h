#pragma once
#ifndef DEF_DICEROLLER
#define DEF_DICEROLLER
#include "Dice.h"
#include <iostream>
using namespace std;

class DiceRoller
{
private:
	Dice diceArray[8];
	int dice_sum;
	int dice_blocked_sum;

public:
	DiceRoller();
	void diceValuesSum();
	void blockingDice();
};

#endif
