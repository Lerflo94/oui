#pragma once
#ifndef DEF_DICE
#define DEF_DICE
#include <string>
using namespace std;

class Dice
{
private:
	int number;
	int value;
	string name;

public:
	Dice();
	int getNumber();
	int getValue();
	string getName();
	void roll();
};

#endif
