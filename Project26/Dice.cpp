#include "Dice.h"

Dice::Dice()
{
	roll();
}

int Dice::getNumber()
{
	return number;
}

int Dice::getValue()
{
	return value;
}

string Dice::getName()
{
	return name;
}

void Dice::roll()
{
	int random_value = rand() % 6;
	number = random_value;
	if (random_value == 0)
	{
		value = 5;
		name = "ver";
	}
	else
	{
		value = random_value;
		name = to_string(random_value);
	}
}

